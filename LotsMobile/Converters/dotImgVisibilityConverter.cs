﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class dotImgVisibilityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string value1 = System.Convert.ToString(values[0]);
            string value2 = System.Convert.ToString(values[1]);
            if ((!string.IsNullOrEmpty(value1) && value1.Trim().ToLower() != "not available") || (!string.IsNullOrEmpty(value2) && value2.Trim().ToLower() != "not available"))
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
