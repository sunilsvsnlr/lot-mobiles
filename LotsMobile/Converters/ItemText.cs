﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class ItemText:IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            LotDBEntity.Models.BasicItem basicItem = value as LotDBEntity.Models.BasicItem;
            if (basicItem != null)
            {
                if (basicItem.OffersAvailable)
                {
                    return "Offer";
                }
                else if (basicItem.EMIAvailable)
                {
                    return "EMI";
                }
                else if (basicItem.FinancingOptionsAvailable)
                {
                    return "Finance";
                }
                else if (basicItem.ExchangeAvailable)
                {
                    return "Exchange";
                }
                else
                {
                    return "";
                }
            }
            return null;    
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
