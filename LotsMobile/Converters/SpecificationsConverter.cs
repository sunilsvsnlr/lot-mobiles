﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class SpecificationsConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] as string != null)
            {
                if (!string.IsNullOrEmpty(values[0].ToString()) && !string.IsNullOrEmpty(values[1].ToString()))

                //if (values[0] as string != null && values[1] as string!=null)
                {
                    return values[0].ToString() + "/" + values[1].ToString();
                }
                else if (!string.IsNullOrEmpty(values[0].ToString()))
                {
                    return values[0].ToString();
                }
            }
            return null;
           
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
