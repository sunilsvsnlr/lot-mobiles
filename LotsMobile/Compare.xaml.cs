﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for Compare.xaml
    /// </summary>
    public partial class Compare : UserControl
    {
        public Compare()
        {
            InitializeComponent();
        }

        Cursor myCustomCursor = new Cursor(new System.IO.MemoryStream(LotsMobile.Properties.Resources.cursor));
        LotDBEntity.Models.BasicItem selectedBasicItem;
        public event EventHandler myEventCompareCartUpdate;
        public event EventHandler myEventCompareDeleteUpdate;
        private void btnFirstlayout_TouchDown_1(object sender, TouchEventArgs e)
        {
            FirstLayout.Visibility = Visibility.Visible;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            lstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
        }

        private void lstParent_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts lstParent Preview TouchDown in Compare Page");
                Image selectedImg = e.OriginalSource as Image;
                if (selectedImg != null)
                {
                    string imgLoc = selectedImg.Source.ToString();
                    selectedBasicItem = Apps.lotContext.BasicItems.Where(c => c.ImageLocation == imgLoc).FirstOrDefault();
                }
                else
                {
                    selectedBasicItem = null;
                }
                LogHelper.Logger.Info("Ends lstParent Preview TouchDown in Compare Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : lstParent_PreviewTouchDown_1: " + ex.Message, ex);
            }
            
            //TouchPoint tpnt = e.GetTouchPoint(lstParent);
            //txtDisplay.Text = "L X: " + tpnt.Position.X + " Y" + tpnt.Position.Y;
        }

        private void lstParent_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts lstParent Preview TouchMove in Compare Page");
                if ((selectedBasicItem != null && !CheckTouchPoints(e, lstParent, 180, 600)) && (CheckTouchPoints(e, SecondLayout, 380, 600) || CheckTouchPoints(e, FirstLayout, 380, 600)))
                {
                    this.Cursor = Cursors.Hand;
                    StoreCompare_Cart.lstCompare.Remove(selectedBasicItem);
                    StoreCompare_Cart.lstCompare.Add(selectedBasicItem);
                    lstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
                }
                else
                {
                    if (this.Cursor != myCustomCursor)
                        this.Cursor = myCustomCursor;
                }
                LogHelper.Logger.Info("Ends lstParent Preview TouchMove in Compare Page");
            }
            catch (Exception ex)
            {
                 LogHelper.Logger.ErrorException("Exception: ComparePage : lstParent_PreviewTouchMove_1: " + ex.Message, ex);
            }
            
        }

        private bool CheckTouchPoints(TouchEventArgs e, IInputElement element, int xMax, int yMax)
        {
            TouchPoint tpnt = e.GetTouchPoint(element);
            LogHelper.Logger.Info("X: " + tpnt.Position.X + " Y: " + tpnt.Position.Y + " XMax: " + xMax + "YMax: " + yMax);
            //txtDisplay.Text = "X: " + tpnt.Position.X + " Y: " + tpnt.Position.Y;
            if (tpnt.Position.X >= 0 && tpnt.Position.X <= xMax && tpnt.Position.Y >= 0 && tpnt.Position.Y <= yMax)
                return true;
            else
                return false;
        }

        private void grdCompareItem_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts  grdCompareItem_PreviewTouchUp_1 in Compare Page ");
                TouchPoint tpnt = e.GetTouchPoint(btnCart);
                //txtDisplay.Text = "X: " + tpnt.Position.X + " Y: " + tpnt.Position.Y;
                if (grdComparedItem != null && CheckTouchPoints(e, btnCart, 130, 60))
                {
                    LotDBEntity.Models.BasicItem baseComparedItem = StoreCompare_Cart.lstCompare.Where(c => c.SNo == grdComparedItem.SNo).SingleOrDefault();
                    StoreCompare_Cart.lstCompare.Remove(baseComparedItem);
                    //StoreCompare_Cart.CmpreCnt -= 1;
                    StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                    myEventCompareCartUpdate(grdComparedItem, null);
                    lstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
                    grdComparedItem = null;
                }
                else if (grdCompareItem != null && CheckTouchPoints(e, btnDelete, 130, 60))
                {
                    LotDBEntity.Models.BasicItem baseComparedItem = StoreCompare_Cart.lstCompare.Where(c => c.SNo == grdComparedItem.SNo).SingleOrDefault();
                    StoreCompare_Cart.lstCompare.Remove(baseComparedItem);
                    //StoreCompare_Cart.CmpreCnt -= 1;
                    StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
                    
                    myEventCompareDeleteUpdate(this, null);
                    lstParent.ItemsSource = StoreCompare_Cart.lstCompare.ToList();
                    grdComparedItem = null;
                }
                this.Cursor = Cursors.Arrow;
                LogHelper.Logger.Info("Ends grdCompareItem_PreviewTouchUp_1 in Compare Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : grdCompareItem_PreviewTouchUp_1: " + ex.Message, ex);
            }
            
        }

        private void grdCompareItem_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts grdCompareItem_PreviewTouchMove_1 in Compare Page");
                if (grdComparedItem != null && (CheckTouchPoints(e, btnCart, 130, 60) || CheckTouchPoints(e, btnDelete, 130, 60)))
                {
                    this.Cursor = Cursors.Hand;
                    if (grdComparedItem == grdBaseItem.DataContext)
                    {
                        stkFirstLayoutWaterMark.Visibility = Visibility.Visible;
                        grdBaseItem.DataContext = null;
                    }
                    else if (grdComparedItem == grdCompareItem.DataContext)
                    {
                        stkSecondLayoutWaterMark.Visibility = Visibility.Visible;
                        grdCompareItem.DataContext = null;
                    }
                }
                else
                {
                    if (this.Cursor != myCustomCursor)
                        this.Cursor = myCustomCursor;
                }
                LogHelper.Logger.Info("Ends grdCompareItem_PreviewTouchMove_1 in Compare Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : grdCompareItem_PreviewTouchMove_1: " + ex.Message, ex);
            }
            
        }

        LotDBEntity.Models.BasicItem grdComparedItem;
        private void grdCompareItem_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts grdCompareItem_PreviewTouchDown_1 in Compare Page");
                if (CheckTouchPoints(e, FirstLayout, 380, 600))
                {
                    grdComparedItem = grdBaseItem.DataContext as LotDBEntity.Models.BasicItem;
                }
                else if (CheckTouchPoints(e, SecondLayout, 380, 600))
                {
                    grdComparedItem = grdCompareItem.DataContext as LotDBEntity.Models.BasicItem;
                }
                LogHelper.Logger.Info("Ends grdCompareItem_PreviewTouchDown_1 in Compare Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : grdCompareItem_PreviewTouchDown_1: " + ex.Message, ex);
            }
            
        }

        private void lstParent_TouchUp_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts lstParent_TouchUp_1 in Compare Page");
                if (selectedBasicItem != null && !CheckTouchPoints(e, lstParent, 380, 600) && CheckTouchPoints(e, FirstLayout, 380, 600))
                {
                    if (grdCompareItem.DataContext != selectedBasicItem)
                    {
                        stkFirstLayoutWaterMark.Visibility = Visibility.Collapsed;
                        grdBaseItem.DataContext = selectedBasicItem;
                    }

                }
                else if (selectedBasicItem != null && !CheckTouchPoints(e, lstParent, 380, 600) && CheckTouchPoints(e, SecondLayout, 380, 600))
                {
                    if (grdBaseItem.DataContext != selectedBasicItem)
                    {
                        stkSecondLayoutWaterMark.Visibility = Visibility.Collapsed;
                        grdCompareItem.DataContext = selectedBasicItem;
                    }
                }
                this.Cursor = Cursors.Arrow;
                LogHelper.Logger.Info("Ends lstParent_TouchUp_1 in Compare Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : lstParent_TouchUp_1: " + ex.Message, ex);
            }
            
        }

        private void lstParent_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        public event EventHandler myEventCmpPhotos;
        private void btnCmpPhotos_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                List<LotDBEntity.Models.BasicItem> lstBasicItems = LoadCompareFields();

                if (lstBasicItems != null)
                {
                    myEventCmpPhotos(lstBasicItems, null);
                    LogHelper.Logger.Info("Ends btnCmpPhotos_TouchDown_1 in Compare Page");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : btnCmpPhotos_TouchDown_1: " + ex.Message, ex);
            }
           
        }

        private List<LotDBEntity.Models.BasicItem> LoadCompareFields()
        {
            LogHelper.Logger.Info("Starts btnCmpPhotos_TouchDown_1 in Compare Page");
            List<LotDBEntity.Models.BasicItem> lstBasicItems = new List<LotDBEntity.Models.BasicItem>();
            LotDBEntity.Models.BasicItem baseItem = grdBaseItem.DataContext as LotDBEntity.Models.BasicItem;
            if (baseItem != null)
                lstBasicItems.Add(baseItem);
            else
            {
                MessageBox.Show("Please drag another item in compare to start.", "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }

            LotDBEntity.Models.BasicItem comparedItem = grdCompareItem.DataContext as LotDBEntity.Models.BasicItem;
            if (comparedItem != null)
                lstBasicItems.Add(comparedItem);
            else
            {
                MessageBox.Show("Please drag another item in compare to start.", "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
                return null;
            }
            return lstBasicItems;
        }

        public event EventHandler myEventCmpVideo;
        private void btnCmpVideo_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts btnCmpVideo_TouchDown_1 in Compare Page");
                List<LotDBEntity.Models.BasicItem> lstBasicItems = LoadCompareFields();
                if (lstBasicItems != null)
                {
                    myEventCmpVideo(lstBasicItems, null);
                    LogHelper.Logger.Info("Ends btnCmpVideo_TouchDown_1 in Compare Page");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePage : btnCmpVideo_TouchDown_1: " + ex.Message, ex);
            }
            
        }
    }
}
