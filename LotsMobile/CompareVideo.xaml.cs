﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for CompareVideo.xaml
    /// </summary>
    public partial class CompareVideo : UserControl
    {
        public event EventHandler myEventCloseVideo;        

        public CompareVideo()
        {
            InitializeComponent();
        }

        private void btnClosePV_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseVideo(this, null);
        }

        //First video code start      
        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            Video1.Volume = (double)volumeSlider1.Value;
        }

        private void Element_MediaEnded(object sender, EventArgs e)
        {
            Video1.Stop();
            ImgVdeo1Pause.Visibility = Visibility.Hidden;
            ImgVdeo1Play.Visibility = Visibility.Visible;
        }

        void InitializePropertyValues()
        {
            Video1.Volume = (double)volumeSlider1.Value;
        }

        private void Vdeo1Play_TouchDown_1(object sender, TouchEventArgs e)
        {
            Video1.Play();
            ImgVdeo1Pause.Visibility = Visibility.Visible;
            ImgVdeo1Play.Visibility = Visibility.Hidden;
            InitializePropertyValues();
        }

        private void Vdeo1Pause_TouchDown_1(object sender, TouchEventArgs e)
        {
            Video1.Pause();
            ImgVdeo1Pause.Visibility = Visibility.Hidden;
            ImgVdeo1Play.Visibility = Visibility.Visible;
        }

        private void Vdeo1Stop_TouchDown_1(object sender, TouchEventArgs e)
        {
            ImgVdeo1Pause.Visibility = Visibility.Hidden;
            ImgVdeo1Play.Visibility = Visibility.Visible;
            Video1.Stop();
        }

        private void Video1_Loaded_1(object sender, RoutedEventArgs e)
        {
            //Video1.ScrubbingEnabled = true;
            Video1.Play();
        }
        //First video code Ended

        //Second video code start
        private void Video2_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            Video2.Stop();
            ImgVdeo2Pause.Visibility = Visibility.Hidden;
            ImgVdeo2Play.Visibility = Visibility.Visible;
        }

        private void Video2_Loaded_1(object sender, RoutedEventArgs e)
        {
            //Video2.ScrubbingEnabled = true;
            Video2.Play();
        }

        private void ImgVdeo2Play_TouchDown_1(object sender, TouchEventArgs e)
        {
            Video2.Play();
            ImgVdeo2Pause.Visibility = Visibility.Visible;
            ImgVdeo2Play.Visibility = Visibility.Hidden;
            InitializePropertyValues2();
        }

        private void ImgVdeo2Pause_TouchDown_1(object sender, TouchEventArgs e)
        {
            Video2.Pause();
            ImgVdeo2Pause.Visibility = Visibility.Hidden;
            ImgVdeo2Play.Visibility = Visibility.Visible;
        }

        private void ImgVdeo2Stop_TouchDown_1(object sender, TouchEventArgs e)
        {
            ImgVdeo2Pause.Visibility = Visibility.Hidden;
            ImgVdeo2Play.Visibility = Visibility.Visible;
            Video2.Stop();
        }

        private void volumeSlider2_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Video2.Volume = (double)volumeSlider2.Value;
        }

        void InitializePropertyValues2()
        {
            Video2.Volume = (double)volumeSlider2.Value;
        }

    }
}
