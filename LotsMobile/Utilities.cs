﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Media.Imaging;

namespace LotsMobile
{
    public static class Utilities
    {
        public static string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Main Lot\LotDB.mdf;Integrated Security=True;Connect Timeout=30";
        public static string storingPath = @"C:\Main Lot";

        public static string getProducts()
        {
            SqlConnection cn = new SqlConnection(connectionString);
            SqlDataAdapter da = new SqlDataAdapter("select * from products", cn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            System.IO.StringWriter strWriter = new System.IO.StringWriter();
            ds.WriteXml(strWriter);

            return strWriter.ToString();
        }

        public static T DeSerializeGeneric<T>(string serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return default(T);
                }
                if (serializedObject.Length == 0)
                {
                    if (default(T) != null)
                        return default(T);
                    return Activator.CreateInstance<T>();
                }
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (StringReader reader = new StringReader(serializedObject))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static BitmapImage ConvertotImg(object value)
        {
            var byteArrayImage = value as byte[];

            if (byteArrayImage != null && byteArrayImage.Length > 0)
            {
                try
                {
                    var ms = new MemoryStream(byteArrayImage);

                    var bitmapImg = new BitmapImage();

                    bitmapImg.BeginInit();
                    bitmapImg.StreamSource = ms;
                    bitmapImg.EndInit();

                    return bitmapImg;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }
    }
}
