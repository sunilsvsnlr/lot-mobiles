﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq.Dynamic;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for Listing.xaml
    /// </summary>
    public partial class Listing : UserControl
    {
        public Listing()
        {
            InitializeComponent();
        }
        public delegate void myEventListing(object sender, EventArgs e);
        public event myEventListing myEventListingItem;
        public delegate void myEventSimilar(object sender, EventArgs e);
        public event myEventSimilar myEventSimilarItem;
        public List<LotDBEntity.Models.BasicItem> lstChoosedItems;
        public int selectedPriceIndex = 0;
        public LotDBEntity.Models.Brands selectedItemBrand = null;
        List<Models.Offers> lstOffers;
        public bool isRefresh = true;
        bool isScrollMoved = false;
        bool isUserControlLoaded = false;
        Button btnTouchedItem = null;


        private void scrollViewerListing_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void btnItem_TouchDown(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {   
                //myEventListingItem(btn, null);
            }
            else
                isScrollMoved = false;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.BasicItems.GroupBy(c => c.ItemBrand).Select(d => d.Key).ToList();
            LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
            brand.Name = "SELECT BRAND";

            lstBrands.Insert(0, brand);
            CboBrand.ItemsSource = lstBrands;

            if (Apps.lotContext.BasicItems != null && lstChoosedItems == null)
                lstChoosedItems = Apps.lotContext.BasicItems.ToList();

            ListingContext.ItemsSource = lstChoosedItems.OrderByDescending(c => c.LastUpdatedTime);
            CheckListingIsEmpty();

            int CountListingItem = ListingContext.Items.Count;

            if (lstChoosedItems.Count > 3 || CountListingItem > 3)
            {
                imgScrollHere.Visibility = Visibility.Visible;
            }
            else
            {
                imgScrollHere.Visibility = Visibility.Hidden;
            }
            scrollViewerListing.ScrollToVerticalOffset(0);

            CboPrice.SelectedIndex = selectedPriceIndex;
            if (selectedItemBrand == null)
                CboBrand.SelectedIndex = 0;
            else
                CboBrand.SelectedItem = selectedItemBrand;
            selectedPriceIndex = 0;
            selectedItemBrand = null;
        }

        public class ComboBoxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void CheckListingIsEmpty()
        {
            if (ListingContext != null)
            {
                int CountListingItem = ListingContext.Items.Count;
                if (CountListingItem == 0)
                    imgNoStock.Visibility = Visibility.Visible;
                else
                    imgNoStock.Visibility = Visibility.Collapsed;
            }
        }

        UserOffers uof = new UserOffers();
        private void CboBrand_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();

            if (CboBrand.SelectedItem != null && CboBrand.SelectedIndex > 0)
            {
                LotDBEntity.Models.Brands brand = CboBrand.SelectedItem as LotDBEntity.Models.Brands;
                if (brand.Name == "SELECT BRAND")
                {
                    ListingContext.ItemsSource = lstChoosedItems;
                    CheckListingIsEmpty();
                }
                else
                {
                    if (ListingContext.ItemsSource != null)
                    {
                        ListItemCount();
                    }
                }
            }
            else
            {
                if (CboBrand != null && CboPrice != null && CboBrand.SelectedIndex == 0 && CboPrice.SelectedIndex == 0 && StoreCompare_Cart.IsFromOfferPage == 0)
                {
                    lstChoosedItems = Apps.lotContext.BasicItems.ToList();
                    ListingContext.ItemsSource = lstChoosedItems;
                    CheckListingIsEmpty();
                }
            }

            //Check CboBrand is empty and bind "SELECT BRAND" Text.
            int count = CboBrand.Items.Count;
            if (count == 0)
            {
                LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                brand.Name = "SELECT BRAND";
                List<LotDBEntity.Models.Brands> lstBrands = new List<LotDBEntity.Models.Brands>();
                lstBrands.Add(brand);
                CboBrand.ItemsSource = lstBrands;
            }
            scrollViewerListing.ScrollToVerticalOffset(0);
        }

        string priceExpression;
        private void ApplyFilter(bool isRefreshBrand = false)
        {
            priceExpression = string.Empty;
            string mainExpression = string.Empty;
            if (CboBrand != null && CboBrand.SelectedItem != null && CboBrand.SelectedIndex > 0)
            {
                LotDBEntity.Models.Brands brand = CboBrand.SelectedItem as LotDBEntity.Models.Brands;
                mainExpression += "ItemBrand.SNo=" + brand.SNo;
            }

            if (CboPrice != null && CboPrice.SelectedItem != null)
            {
                if (CboPrice.SelectedIndex > 0)
                {
                    if (!string.IsNullOrEmpty(mainExpression))
                    {
                        mainExpression += " and ";
                    }
                    if (CboPrice.SelectedIndex == 1)
                        priceExpression += "DefaultPrice<5000";
                    else if (CboPrice.SelectedIndex == 2)
                        priceExpression += "DefaultPrice>=5000 and DefaultPrice<=10000";
                    else if (CboPrice.SelectedIndex == 3)
                        priceExpression += "DefaultPrice>10000 and DefaultPrice<=15000";
                    else if (CboPrice.SelectedIndex == 4)
                        priceExpression += "DefaultPrice>15000 and DefaultPrice<=25000";
                    else if (CboPrice.SelectedIndex == 5)
                        priceExpression += "DefaultPrice>25000 and DefaultPrice<=35000";
                    else if (CboPrice.SelectedIndex == 6)
                        priceExpression += "DefaultPrice>35000 and DefaultPrice<=45000";
                    else if (CboPrice.SelectedIndex == 7)
                        priceExpression += "DefaultPrice>45000 and DefaultPrice<=55000";
                    else if (CboPrice.SelectedIndex == 8)
                        priceExpression += "DefaultPrice>55000";

                    mainExpression += priceExpression;
                }
            }

            if (isRefresh)
            {
                if (!string.IsNullOrEmpty(mainExpression))
                {
                    lstChoosedItems = Apps.lotContext.BasicItems.AsQueryable().Where(mainExpression).ToList();
                    ListingContext.ItemsSource = lstChoosedItems;

                    if (ListingContext.ItemsSource != null)
                    {
                        ListItemCount();
                    }
                }
                else if (Apps.lotContext.BasicItems != null && Apps.lotContext.BasicItems.ToList().Count > 0 && ListingContext != null)
                {
                    lstChoosedItems = Apps.lotContext.BasicItems.ToList();
                    ListingContext.ItemsSource = lstChoosedItems;

                    if (ListingContext.ItemsSource != null)
                    {
                        ListItemCount();
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mainExpression))
                {
                    lstChoosedItems = Apps.lotContext.BasicItems.AsQueryable().Where(mainExpression).ToList();
                    ListingContext.ItemsSource = lstChoosedItems;
                }
                else if (lstChoosedItems != null && lstChoosedItems.Count > 0 && ListingContext != null)
                {
                    ListingContext.ItemsSource = lstChoosedItems;
                }

                if (ListingContext.ItemsSource != null)
                {
                    ListItemCount();
                }
            }
            if (ListingContext != null && ListingContext.ItemsSource != null)
            {
                if (!string.IsNullOrEmpty(priceExpression))
                {
                    List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.AsQueryable().Where(priceExpression).ToList();
                    if (lstBasicItems != null && lstBasicItems.Count > 0)
                    {
                        if (isRefreshBrand)
                        {
                            LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                            brand.Name = "SELECT BRAND";

                            List<LotDBEntity.Models.Brands> lstFilteredBrands = lstBasicItems.GroupBy(c => c.ItemBrand).Select(c => c.Key).ToList();
                            lstFilteredBrands.Insert(0, brand);

                            if (CboBrand != null)
                            {
                                CboBrand.ItemsSource = lstFilteredBrands.ToList();
                                CboBrand.SelectedIndex = 0;
                            }
                        }
                    }
                    else
                        CboBrand.ItemsSource = null;
                }
            }
            else
            {
                if (isRefreshBrand)
                {
                    LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                    brand.Name = "SELECT BRAND";
                    List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.BasicItems.GroupBy(c => c.ItemBrand).Select(d => d.Key).ToList();
                    lstBrands.Insert(0, brand);
                    if (CboBrand != null)
                    {
                        CboBrand.ItemsSource = lstBrands;
                        CboBrand.SelectedIndex = 0;
                    }
                }
            }
            CheckListingIsEmpty();
        }

        private void ListItemCount()
        {
            int CountListingItem = ListingContext.Items.Count;
            if (CountListingItem > 3)
            {
                imgScrollHere.Visibility = Visibility.Visible;
            }
            else
            {
                imgScrollHere.Visibility = Visibility.Hidden;
            }
        }

        private void CboPrice_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter(true);
            if (CboBrand != null && CboPrice != null && CboBrand.SelectedIndex == 0 && CboPrice.SelectedIndex == 0)
            {
                lstChoosedItems = Apps.lotContext.BasicItems.ToList();
                ListingContext.ItemsSource = lstChoosedItems;
                CheckListingIsEmpty();
                LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                brand.Name = "SELECT BRAND";
                List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.BasicItems.GroupBy(c => c.ItemBrand).Select(d => d.Key).ToList();
                lstBrands.Insert(0, brand);
                if (CboBrand != null)
                {
                    CboBrand.ItemsSource = lstBrands;
                    CboBrand.SelectedIndex = 0;
                }
                else
                {
                    brand.Name = "SELECT BRAND";
                    lstBrands.Insert(0, brand);
                    CboBrand.ItemsSource = lstBrands;
                    CboBrand.SelectedIndex = 0;
                }
            }
            else if (CboBrand == null && CboPrice != null)
            {
                //CboBrand.ItemsSource = null;
                //LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                //brand.Name = "SELECT BRAND";
                //List<LotDBEntity.Models.Brands> lstBrands = new List<LotDBEntity.Models.Brands>();
                //lstBrands.Insert(0, brand);
                //CboBrand.ItemsSource = lstBrands;
                //CboBrand.SelectedIndex = 0;
            }
            else if(CboBrand ==null && CboPrice ==null)
            {
                LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                brand.Name = "SELECT BRAND";
                List<LotDBEntity.Models.Brands> lstBrands = new List<LotDBEntity.Models.Brands>();
                lstBrands.Insert(0, brand);
                CboBrand.ItemsSource = lstBrands;
                CboBrand.SelectedIndex = 0;
            }

        }

        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private void scrollViewerListing_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Listing: scrollViewerListing_PreviewTouchUp_1(...): Starts");
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                LogHelper.Logger.Info("Y: " + touchScrollStartPoint.Position.Y);
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    myEventListingItem(btnTouchedItem, null);
                }
            }
        }

        private void scrollViewerListing_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Listing: scrollViewerListing_PreviewTouchDown_1(...): Starts");
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
            if (touchScrollStartPoint != null)
            {
                LogHelper.Logger.Info("Listing: touchScrollStartPoint:" + touchScrollStartPoint.Position.X);
                LogHelper.Logger.Info("Listing: touchScrollStartPoint:" + touchScrollStartPoint.Position.Y);
            }

            if (touchScrollEndPoint != null)
            {
                LogHelper.Logger.Info("Listing: touchScrollEndPoint:" + touchScrollEndPoint.Position.X);
                LogHelper.Logger.Info("Listing: touchScrollEndPoint:" + touchScrollEndPoint.Position.Y);
            }
        }

        private void scrollViewerListing_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }
    }
}
