﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for ItemView.xaml
    /// </summary>
    public partial class ItemView : UserControl
    {
        public event EventHandler myEventCartCntUpdate;
        public delegate void myEventListing(object sender, EventArgs e);
        public event myEventListing myEventListingItem;
        public event EventHandler MyEventCmpreCntUpdate;
        public LotDBEntity.Models.SimilarProducts SimilarItem { get; set; }
        public event EventHandler launchSimilarProduct;
        public event EventHandler refreshIsfromCartVal;
        public event EventHandler launchAccessory;
        public LotDBEntity.Models.BasicItem BasicItem { get; set; }
        public bool isFromCart = false;
        public event EventHandler myEventCartRefresh;

        public ItemView()
        {
            InitializeComponent();
        }

        private void btnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts btnCart_TouchDown_1 in ItemViewPage");
                if (Convert.ToString(btnCart.Content) == "Remove")
                {
                    LotDBEntity.Models.BasicItem cartItem = StoreCompare_Cart.lstCart.Where(c => c.SNo == BasicItem.SNo).SingleOrDefault();
                    StoreCompare_Cart.lstCart.Remove(cartItem);
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                    this.Visibility = Visibility.Collapsed;
                }
                else
                {
                    StoreCompare_Cart.AddCart<LotDBEntity.Models.BasicItem>(BasicItem, sno: BasicItem.SNo);
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                    this.Visibility = Visibility.Collapsed;
                }
                LogHelper.Logger.Info("Ends btnCart_TouchDown_1 in ItemViewPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : btnCart_TouchDown_1: " + ex.Message, ex);
            }

        }

        private void btnCompare_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts btnCompare_TouchDown_1 in ItemViewPage");
                bool issuccess = false;
                grdItemView.DataContext = BasicItem;
                List<LotDBEntity.Models.BasicItem> Content = Apps.lotContext.BasicItems.Where(c => c.SNo == BasicItem.SNo).ToList();

                foreach (LotDBEntity.Models.BasicItem lstSample in Content)
                {
                    List<LotDBEntity.Models.BasicItem> lstprod = StoreCompare_Cart.lstCompare.Where(c => c.SNo.Equals(lstSample.SNo)).ToList();
                    if (lstprod.Count == 0)
                    {
                        issuccess = true;
                        StoreCompare_Cart.lstCompare.Add(lstSample);
                        StoreCompare_Cart.CmpreCnt += 1;
                        MyEventCmpreCntUpdate(this, null);
                    }
                    else
                    {
                        MessageBox.Show("Already added the selected item to compare. Please choose another one....", "My Fashions", MessageBoxButton.OK, MessageBoxImage.Information);

                        this.Visibility = Visibility.Hidden;
                    }
                }

                if (issuccess)
                {
                    System.Threading.Thread.Sleep(500);
                    this.Visibility = Visibility.Hidden;
                }
                LogHelper.Logger.Info("Ends btnCompare_TouchDown_1 in ItemViewPage");

            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : btnCompare_TouchDown_1: " + ex.Message, ex);
            }
        }

        private void btnUserRating_TouchDown_1(object sender, TouchEventArgs e)
        {
            if (UserRatingPopup.Visibility == Visibility.Visible)
            {
                UserRatingPopup.Visibility = Visibility.Collapsed;
            }
            else
            {
                UserRatingPopup.Visibility = Visibility.Visible;
            }
        }

        private void btnSaveRating_TouchDown_1(object sender, TouchEventArgs e)
        {
            UserRatingPopup.Visibility = Visibility.Collapsed;
        }

        private string storePrevbtnName;
        private void btnBanner_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btnSelectedItem = sender as Button;

            if (btnSelectedItem.Name == "btnBanner")
            {
                OfferBannerPopup.Margin = new Thickness(-10, 35, 0, 0);
                txtPromotions.Text = BasicItem.Offer;
                if (storePrevbtnName == btnSelectedItem.Name)
                {
                    if (OfferBannerPopup.Visibility == Visibility.Hidden)
                        OfferBannerPopup.Visibility = Visibility.Visible;
                    else
                        OfferBannerPopup.Visibility = Visibility.Hidden;
                }
                else if (OfferBannerPopup.Visibility == Visibility.Hidden)
                    OfferBannerPopup.Visibility = Visibility.Visible;
            }
            else if (btnSelectedItem.Name == "btnEMI")
            {
                OfferBannerPopup.Margin = new Thickness(-10, 65, 0, 0);
                txtPromotions.Text = BasicItem.EMIMonths;
                if (storePrevbtnName == btnSelectedItem.Name)
                {
                    if (OfferBannerPopup.Visibility == Visibility.Hidden)
                        OfferBannerPopup.Visibility = Visibility.Visible;
                    else
                        OfferBannerPopup.Visibility = Visibility.Hidden;
                }
                else if (OfferBannerPopup.Visibility == Visibility.Hidden)
                    OfferBannerPopup.Visibility = Visibility.Visible;
            }
            else if (btnSelectedItem.Name == "btnExchange")
            {
                OfferBannerPopup.Margin = new Thickness(-10, 100, 0, 0);
                txtPromotions.Text = BasicItem.ExchangeDetails;
                if (storePrevbtnName == btnSelectedItem.Name)
                {
                    if (OfferBannerPopup.Visibility == Visibility.Hidden)
                        OfferBannerPopup.Visibility = Visibility.Visible;
                    else
                        OfferBannerPopup.Visibility = Visibility.Hidden;
                }
                else if (OfferBannerPopup.Visibility == Visibility.Hidden)
                    OfferBannerPopup.Visibility = Visibility.Visible;
            }
            else if (btnSelectedItem.Name == "btnFinance")
            {
                OfferBannerPopup.Margin = new Thickness(-10, 135, 0, 0);
                txtPromotions.Text = BasicItem.FinancerDetails;
                if (storePrevbtnName == btnSelectedItem.Name)
                {
                    if (OfferBannerPopup.Visibility == Visibility.Hidden)
                        OfferBannerPopup.Visibility = Visibility.Visible;
                    else
                        OfferBannerPopup.Visibility = Visibility.Hidden;
                }
                else if (OfferBannerPopup.Visibility == Visibility.Hidden)
                    OfferBannerPopup.Visibility = Visibility.Visible;
            }
            storePrevbtnName = btnSelectedItem.Name;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts UserControl_Loaded_1 in ItemViewPage");
                UserComments.ItemsSource = BasicItem.LstComments.ToList();
                this.DataContext = BasicItem;
                if (BasicItem.InternalStorage != "NOT AVAILABLE" || BasicItem.ExternalStorage != "NOT AVAILABLE")
                {
                    LabelMemory.Visibility = Visibility.Visible;
                }
                if (BasicItem.PrimaryCamera != "NOT AVAILABLE" || BasicItem.SecondaryCamera != "NOT AVAILABLE")
                {
                    LabelCamera.Visibility = Visibility.Visible;
                }
                stksimilar.DataContext = BasicItem;

                if (BasicItem.LstAccessories.Count == 0)
                {
                    cmpPhone.Text = "";
                }
                if (BasicItem.LstSimilarProducts.Count == 0)
                {
                    SimilarPhones.Text = "";
                }
                if (isFromCart)
                {
                    if (StoreCompare_Cart.lstCart.ToList().Exists(c => c.SNo == BasicItem.SNo))
                    {
                        btnCart.Content = "Remove";
                        btnCompare.Visibility = Visibility.Collapsed;
                    }
                }

                if (string.IsNullOrEmpty(BasicItem.OSName))
                {
                    stkOS.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.ProcessorType))
                {
                    stkProcessor.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.DisplayType))
                {
                    stkDisplay.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.PrimaryCamera))
                {
                    stkCamera.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.PrimaryCamera) && string.IsNullOrEmpty(BasicItem.SecondaryCamera))
                {
                    stkMemory.Visibility = Visibility.Collapsed;
                }
                LogHelper.Logger.Info("Ends UserControl_Loaded_1 in ItemViewPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : UserControl_Loaded_1: " + ex.Message, ex);
            }

        }

        private void BtnItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts BtnItem_TouchDown_1 in ItemViewPage ");
                Button btnSelectedItem = (Button)sender;
                if (btnSelectedItem != null)
                {
                    LotDBEntity.Models.ImageCollection selectedImg = (LotDBEntity.Models.ImageCollection)btnSelectedItem.DataContext;
                    if (selectedImg != null)
                    {
                        stkPnlSelectedImg.DataContext = selectedImg;
                    }

                }
                LogHelper.Logger.Info("Ends BtnItem_TouchDown_1 in ItemViewPage ");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : BtnItem_TouchDown_1: " + ex.Message, ex);
            }

        }

        private void BtnSimilarItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts BtnSimilarItem_TouchDown_1 in ItemViewPage ");
                Button btnSimilarProdContext = sender as Button;
                LotDBEntity.Models.SimilarProducts basicSimilarItem = btnSimilarProdContext.DataContext as LotDBEntity.Models.SimilarProducts;
                if (basicSimilarItem != null)
                {

                    long val = Convert.ToInt64(basicSimilarItem.ItemSKU);
                    var basicItem = Apps.lotContext.BasicItems.Where(c => c.SNo == val).FirstOrDefault();
                    btnSimilarProdContext.DataContext = basicItem;
                    if (isFromCart)
                        refreshIsfromCartVal(this, null);

                    launchSimilarProduct(btnSimilarProdContext, null);
                    //myEventListingItem(btnSimilarProdContext.DataContext, null);
                }
                else
                {
                    if (isFromCart)
                        refreshIsfromCartVal(this, null);
                    launchSimilarProduct(btnSimilarProdContext, null);
                }
                LogHelper.Logger.Info("Ends BtnSimilarItem_TouchDown_1 in ItemViewPage ");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : BtnSimilarItem_TouchDown_1: " + ex.Message, ex);
            }
        }
        private void BtnAccessory_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts BtnAccessory_TouchDown_1 in ItemViewPage");
                Button btnSelectedAccessory = sender as Button;
                LotDBEntity.Models.BasicAccessories basicAccessory = (LotDBEntity.Models.BasicAccessories)btnSelectedAccessory.DataContext;
                List<object> lstObj = new List<object>();
                lstObj.Add(basicAccessory);
                lstObj.Add(BasicItem.SNo);
                lstObj.Add(isFromCart);
                launchAccessory(lstObj, null);
                LogHelper.Logger.Info("Ends BtnAccessory_TouchDown_1 in ItemViewPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : BtnAccessory_TouchDown_1: " + ex.Message, ex);
            }

        }
        public event EventHandler closeBtnClicked;
        private void btnClose_TouchDown_2(object sender, TouchEventArgs e)
        {
            closeBtnClicked(this, null);
        }

        private void btnWriteComment_TouchDown_1(object sender, TouchEventArgs e)
        {
            UserComment userComment = new UserComment();
            userComment.basicItem = BasicItem;
            userComment.refreshComments += userComment_refreshComments;
            userComment.ShowDialog();
        }

        void userComment_refreshComments(object sender, EventArgs e)
        {
            UserComments.ItemsSource = BasicItem.LstComments.ToList();
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
