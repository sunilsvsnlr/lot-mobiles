﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for AccessoryListing.xaml
    /// </summary>
    
    public partial class AccessoryListing : UserControl
    {
        bool isScrollMoved = false;
        Button btnTouchedItem = null;
        public delegate void myEventAccListing(object sender, EventArgs e);
        public event myEventAccListing myEventAccListingItem;
        public AccessoryListing()
        {
            InitializeComponent();
        }
        private void CheckListingIsEmpty()
        {
            if (ListingContext != null)
            {
                int CountListingItem = ListingContext.Items.Count;
                if (CountListingItem == 0)
                    imgNoStock.Visibility = Visibility.Visible;
                else
                    imgNoStock.Visibility = Visibility.Collapsed;
            }
        }
        private void ListItemCount()
        {
            int CountListingItem = ListingContext.Items.Count;
            if (CountListingItem > 3)
            {
                imgScrollHere.Visibility = Visibility.Visible;
            }
            else
            {
                imgScrollHere.Visibility = Visibility.Hidden;
            }
        }
        private void CboBrand_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            LotDBEntity.Models.Brands brand = CboBrand.SelectedItem as LotDBEntity.Models.Brands;
            if (brand.Name == "SELECT BRAND")
            {
                ListingContext.ItemsSource = Apps.lotContext.BasicAccessories.ToList();
                ListItemCount();
            }
            else
            {
                ListingContext.ItemsSource = Apps.lotContext.BasicAccessories.Where(c => c.brand.Name == brand.Name).ToList();
                ListItemCount();
            }
        }

        private void scrollViewerListing_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }
        private void scrollViewerListing_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private void scrollViewerListing_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    myEventAccListingItem(btnTouchedItem, null);
                }
            }
        }

        private void scrollViewerListing_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }
        private void scrollViewerListing_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void BtnItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {
                //myEventAccListingItem(this, null);
            }
            else
                isScrollMoved = false;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.Brands.Where(c=>c.LstAccessories.Count>0).ToList();
            LotDBEntity.Models.Brands brand=new  LotDBEntity.Models.Brands();
            brand.Name="SELECT BRAND";
            lstBrands.Insert(0, brand);
            CboBrand.ItemsSource = lstBrands;
            ListingContext.ItemsSource = Apps.lotContext.BasicAccessories.ToList();

            CheckListingIsEmpty();
        }
    }
}
