﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for ValueAddedServises.xaml
    /// </summary>
    public partial class ValueAddedServises : UserControl
    {
        public event EventHandler myEventVAStoHome;
        public LotDBEntity.Models.BasicItem BasicItem { get; set; }

        public ValueAddedServises()
        {
            InitializeComponent();
        }

        private void btnBackToHome_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            myEventVAStoHome(this, null);
        }
        
        
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<LotDBEntity.Models.VASImageCollection> lstVASImgs = Apps.lotContext.VASImgCollection.ToList();
            this.DataContext = lstVASImgs;            
        }

        private void scrollViewerListing_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
