﻿#pragma checksum "..\..\MainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "543F68C1A49EAFA5E63E30369951CA0A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LotsMobile {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 89 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid NavigationBtnPanel;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOfferOfTheDay;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnShopMore;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFindAccessories;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkCartCompareBtnPanel;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCart;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCartCount;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCompare;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCompareCount;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid parentPanel;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HomePanel;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid SliderPanel;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image1;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image2;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image3;
        
        #line default
        #line hidden
        
        
        #line 197 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image4;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgBg;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkButtonPanel;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button grdFindMatch;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgMatch;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button grdOffers;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgOffer;
        
        #line default
        #line hidden
        
        
        #line 256 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button grdAccessories;
        
        #line default
        #line hidden
        
        
        #line 258 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgAccessories;
        
        #line default
        #line hidden
        
        
        #line 274 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button grdServices;
        
        #line default
        #line hidden
        
        
        #line 276 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgServices;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border UserCommentPanel;
        
        #line default
        #line hidden
        
        
        #line 327 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUserName;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUserContactNo;
        
        #line default
        #line hidden
        
        
        #line 329 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUserComment;
        
        #line default
        #line hidden
        
        
        #line 333 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCommentSave;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCloseComment;
        
        #line default
        #line hidden
        
        
        #line 349 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnHome;
        
        #line default
        #line hidden
        
        
        #line 355 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdminSetting;
        
        #line default
        #line hidden
        
        
        #line 364 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border syncProcessPanel;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCloud;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgRefresh;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar PrgLoadingBar;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblLoadingText;
        
        #line default
        #line hidden
        
        
        #line 392 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HomePanelVideo;
        
        #line default
        #line hidden
        
        
        #line 393 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MediaElement HomeVideo;
        
        #line default
        #line hidden
        
        
        #line 396 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border brdVideoSkip;
        
        #line default
        #line hidden
        
        
        #line 406 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Settings;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\MainWindow.xaml"
            ((LotsMobile.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.NavigationBtnPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btnOfferOfTheDay = ((System.Windows.Controls.Button)(target));
            
            #line 91 "..\..\MainWindow.xaml"
            this.btnOfferOfTheDay.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnOfferOfTheDay_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnShopMore = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\MainWindow.xaml"
            this.btnShopMore.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnShopMore_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnFindAccessories = ((System.Windows.Controls.Button)(target));
            
            #line 101 "..\..\MainWindow.xaml"
            this.btnFindAccessories.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnFindAccessories_PreviewTouchUp_1);
            
            #line default
            #line hidden
            return;
            case 6:
            this.stkCartCompareBtnPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 7:
            this.btnCart = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\MainWindow.xaml"
            this.btnCart.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCart_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lblCartCount = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.btnCompare = ((System.Windows.Controls.Button)(target));
            
            #line 132 "..\..\MainWindow.xaml"
            this.btnCompare.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCompare_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 10:
            this.lblCompareCount = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.parentPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.HomePanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 13:
            this.SliderPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.image1 = ((System.Windows.Controls.Image)(target));
            return;
            case 15:
            this.image2 = ((System.Windows.Controls.Image)(target));
            return;
            case 16:
            this.image3 = ((System.Windows.Controls.Image)(target));
            return;
            case 17:
            this.image4 = ((System.Windows.Controls.Image)(target));
            return;
            case 18:
            this.imgBg = ((System.Windows.Controls.Image)(target));
            
            #line 214 "..\..\MainWindow.xaml"
            this.imgBg.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.imgBg_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 19:
            this.stkButtonPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 20:
            this.grdFindMatch = ((System.Windows.Controls.Button)(target));
            
            #line 219 "..\..\MainWindow.xaml"
            this.grdFindMatch.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.grdFindMatch_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 21:
            this.imgMatch = ((System.Windows.Controls.Image)(target));
            return;
            case 22:
            this.grdOffers = ((System.Windows.Controls.Button)(target));
            
            #line 238 "..\..\MainWindow.xaml"
            this.grdOffers.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.grdOffers_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 23:
            this.imgOffer = ((System.Windows.Controls.Image)(target));
            return;
            case 24:
            this.grdAccessories = ((System.Windows.Controls.Button)(target));
            
            #line 256 "..\..\MainWindow.xaml"
            this.grdAccessories.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.grdAccessories_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 25:
            this.imgAccessories = ((System.Windows.Controls.Image)(target));
            return;
            case 26:
            this.grdServices = ((System.Windows.Controls.Button)(target));
            
            #line 274 "..\..\MainWindow.xaml"
            this.grdServices.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.grdServices_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 27:
            this.imgServices = ((System.Windows.Controls.Image)(target));
            return;
            case 28:
            this.UserCommentPanel = ((System.Windows.Controls.Border)(target));
            return;
            case 29:
            this.txtUserName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.txtUserContactNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.txtUserComment = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.btnCommentSave = ((System.Windows.Controls.Button)(target));
            
            #line 333 "..\..\MainWindow.xaml"
            this.btnCommentSave.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCommentSave_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 33:
            this.btnCloseComment = ((System.Windows.Controls.Button)(target));
            
            #line 339 "..\..\MainWindow.xaml"
            this.btnCloseComment.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCloseComment_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 34:
            this.btnHome = ((System.Windows.Controls.Button)(target));
            
            #line 349 "..\..\MainWindow.xaml"
            this.btnHome.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnHome_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 35:
            this.btnAdminSetting = ((System.Windows.Controls.Button)(target));
            
            #line 355 "..\..\MainWindow.xaml"
            this.btnAdminSetting.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnAdminSetting_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 36:
            this.syncProcessPanel = ((System.Windows.Controls.Border)(target));
            return;
            case 37:
            this.imgCloud = ((System.Windows.Controls.Image)(target));
            return;
            case 38:
            this.imgRefresh = ((System.Windows.Controls.Image)(target));
            return;
            case 39:
            this.PrgLoadingBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 40:
            this.lblLoadingText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.HomePanelVideo = ((System.Windows.Controls.Grid)(target));
            return;
            case 42:
            this.HomeVideo = ((System.Windows.Controls.MediaElement)(target));
            
            #line 393 "..\..\MainWindow.xaml"
            this.HomeVideo.MediaEnded += new System.Windows.RoutedEventHandler(this.HomeVideo_MediaEnded);
            
            #line default
            #line hidden
            return;
            case 43:
            this.brdVideoSkip = ((System.Windows.Controls.Border)(target));
            
            #line 396 "..\..\MainWindow.xaml"
            this.brdVideoSkip.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.brdVideoSkip_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 44:
            this.Settings = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

