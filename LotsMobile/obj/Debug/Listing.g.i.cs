﻿#pragma checksum "..\..\Listing.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3A33A5F90D00C4A46B9789F0F3009525"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LotsMobile.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace LotsMobile {
    
    
    /// <summary>
    /// Listing
    /// </summary>
    public partial class Listing : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 57 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid listingLayout;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtOffer;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CboPrice;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CboBrand;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgScrollHere;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollViewerListing;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ListingContext;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\Listing.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgNoStock;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/listing.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Listing.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\Listing.xaml"
            ((LotsMobile.Listing)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 3:
            this.listingLayout = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.txtOffer = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.CboPrice = ((System.Windows.Controls.ComboBox)(target));
            
            #line 71 "..\..\Listing.xaml"
            this.CboPrice.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboPrice_SelectionChanged_1);
            
            #line default
            #line hidden
            return;
            case 6:
            this.CboBrand = ((System.Windows.Controls.ComboBox)(target));
            
            #line 88 "..\..\Listing.xaml"
            this.CboBrand.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboBrand_SelectionChanged_1);
            
            #line default
            #line hidden
            return;
            case 7:
            this.imgScrollHere = ((System.Windows.Controls.Image)(target));
            return;
            case 8:
            this.scrollViewerListing = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 102 "..\..\Listing.xaml"
            this.scrollViewerListing.PreviewTouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.scrollViewerListing_PreviewTouchDown_1);
            
            #line default
            #line hidden
            
            #line 103 "..\..\Listing.xaml"
            this.scrollViewerListing.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.scrollViewerListing_PreviewTouchUp_1);
            
            #line default
            #line hidden
            
            #line 103 "..\..\Listing.xaml"
            this.scrollViewerListing.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.scrollViewerListing_PreviewTouchMove_1);
            
            #line default
            #line hidden
            
            #line 104 "..\..\Listing.xaml"
            this.scrollViewerListing.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.scrollViewerListing_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            
            #line 104 "..\..\Listing.xaml"
            this.scrollViewerListing.ScrollChanged += new System.Windows.Controls.ScrollChangedEventHandler(this.scrollViewerListing_ScrollChanged_1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ListingContext = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 10:
            this.imgNoStock = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 13 "..\..\Listing.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnItem_TouchDown);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

